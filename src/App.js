import "./App.scss";
import {Component} from "react";
import ProductsList from "./components/ProductsList/ProductsList";
import Header from "./components/Header/Header";

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            inCartAmount: 0,
            inFavorAmount: 0
        }
    }

    componentDidMount() {
        const cart = JSON.parse(localStorage.getItem('productsInCart'));
        const favor = JSON.parse(localStorage.getItem('productsInFavor'));
        this.setState({inCartAmount: cart.length, inFavorAmount: favor.length})
    }

    setCartAmount = (step) => {
        this.setState({inCartAmount: this.state.inCartAmount + step})
    }

    setFavorAmount = (step) => {
        console.log(step);
        this.setState( {inFavorAmount: this.state.inFavorAmount + step})
    }

    render() {
        const {inFavorAmount, inCartAmount} = this.state;
        return <>
            <Header inFavorAmount = {inFavorAmount} inCartAmount = {inCartAmount}/>
            <ProductsList updateFavor = {this.setFavorAmount} updateCart = {this.setCartAmount}/>
        </>;
    }
}

export default App;
