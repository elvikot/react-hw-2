import { Component } from "react";
import "./Product.scss";
import PropTypes from "prop-types";
import Button from "../Button/Button";

export default class Product extends Component {
  constructor(props) {
    super(props);
  }

  addToCart = () => {
    const { title, url, vendorCode, price, color } = this.props;
    const data = {
      title,
      url,
      vendorCode,
      price,
      color,
    };
    this.props.openModal(data);
  };

  removeFromCart = () => {
    const { title, url, vendorCode, price, color } = this.props;
    const data = {
      title,
      url,
      vendorCode,
      price,
      color,
    };
    this.props.removeFromCart(data);
  };

  addToFavor = () => {
    const { title, url, vendorCode, price, color } = this.props;
    const data = {
      title,
      url,
      vendorCode,
      price,
      color,
    };
    this.props.addToFavor(data);
  };

  removeFromFavor = () => {
    const { title, url, vendorCode, price, color } = this.props;
    const data = {
      title,
      url,
      vendorCode,
      price,
      color,
    };
    this.props.removeFromFavor(data);
  };

  render() {
    const { title, url, vendorCode, price, color, inCart, inFavor } =
      this.props;

    return (
      <div className="product-item">
        {inFavor ? (
          <span className="star active" onClick={this.removeFromFavor}></span>
        ) : (
          <span className="star" onClick={this.addToFavor}></span>
        )}
        <div className="product-item__img-wrapper">
          <img alt="" className="product-item__img" src={url} />
        </div>
        <div className="product-item__info">
          <p className="product-item__title">{title}</p>
          <p className="product-item__vendor-code">Vendor code: {vendorCode}</p>
          <p className="product-item__color">Color: {color}</p>
          <p className="product-item__price">₴{price}</p>
          {inCart ? (
            <Button
              className="product-item__cart added"
              text="Remove"
              onClick={this.removeFromCart}
            />
          ) : (
            <Button
              className="product-item__cart"
              text="Add to cart"
              onClick={this.addToCart}
            />
          )}
        </div>
      </div>
    );
  }
}

Product.propTypes = {
  title: PropTypes.string.isRequired,
  url: PropTypes.string.isRequired,
  vendorCode: PropTypes.string.isRequired,
  price: PropTypes.number.isRequired,
  color: PropTypes.string.isRequired,
};
