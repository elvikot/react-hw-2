import Favor from "../Favor/Favor";
import Cart from "../Cart/Cart";
import {Component} from "react";
import './Header.scss';

export default class Header extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        const {inFavorAmount, inCartAmount} = this.props;
        return <header className="header">
            <Favor productsInFavorAmount={inFavorAmount}/>
            <Cart productInCartAmount={inCartAmount}/>
        </header>
    }
}