import { Component } from "react";
import { faShoppingCart } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import "./Cart.scss";
import PropTypes from "prop-types";
import Favor from "../Favor/Favor";

export default class Cart extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { productInCartAmount } = this.props;
    return (
      <div className="wrapper">
        <p className="counter">{productInCartAmount}</p>
        <FontAwesomeIcon icon={faShoppingCart} />
      </div>
    );
  }
}

Cart.propTypes = {
  productInCartAmount: PropTypes.number.isRequired,
};
