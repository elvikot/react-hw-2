import { Component } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCircleXmark } from "@fortawesome/free-solid-svg-icons";
import Button from "../Button/Button";
import "./Modal.scss";
import PropTypes from "prop-types";
import Product from "../Product/Product";

export default class Modal extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { title, content, btns } = this.props;
    return (
      <div className="portal" onClick={this.props.closeModal}>
        <div className="portal-content">
          <h3 className="title">{title}</h3>
          <p>{content}</p>
          <div className="btn-wrapper">
            <Button
              className="btn"
              onClick={this.props.addProduct}
              text={btns[0]}
            />
            <Button
              className="btn"
              onClick={this.props.closeModal}
              text={btns[1]}
            />
          </div>
          <span className="close-btn" onClick={this.props.closeModal}>
            &#11198;
          </span>
        </div>
      </div>
    );
  }
}

Modal.propTypes = {
  title: PropTypes.string.isRequired,
  content: PropTypes.string.isRequired,
  btns: PropTypes.array.isRequired,
  addProduct: PropTypes.func.isRequired,
  closeModal: PropTypes.func.isRequired,
};
