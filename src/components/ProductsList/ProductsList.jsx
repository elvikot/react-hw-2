import "./ProductsList.scss";
import Favor from "../Favor/Favor";
import Cart from "../Cart/Cart";
import Product from "../Product/Product";
import Modal from "../Modal/Modal";
import { Component } from "react";

export default class ProductsList extends Component {
  constructor(props) {
    super(props);

    this.state = {
      productsArr: [],
      productsInCart: "",
      productsInFavor: "",
      cart: [],
      favor: [],
      openedModalStatus: "",
      selectedProduct: "",
    };
  }
  componentDidMount() {
    fetch("./products.json")
      .then((res) => res.json())
      .then((data) => this.setState({ productsArr: data }));

    const cart = JSON.parse(localStorage.getItem("productsInCart"));
    if (!cart) {
      localStorage.setItem("productsInCart", JSON.stringify([]));
      this.setState({ productsInCart: 0 });
    } else {
      this.setState({ productsInCart: cart.length });
    }

    const favor = JSON.parse(localStorage.getItem("productsInFavor"));
    if (!favor) {
      localStorage.setItem("productsInFavor", JSON.stringify([]));
      this.setState({ productsInFavor: 0 });
    } else {
      this.setState({ productsInFavor: favor.length });
    }
  }

  closeModal = (e) => {
    e.preventDefault();
    if (e.target === e.currentTarget) {
      this.setState({ openedModalStatus: false });
      document.body.style.overflow = "unset";
    }
  };

  openModal = (product) => {
    this.setState({ openedModalStatus: true, selectedProduct: product });
    document.body.style.overflow = "hidden";
  };

  addToCart = () => {
    const data = JSON.parse(localStorage.getItem("productsInCart"));
    data.push(this.state.selectedProduct);
    localStorage.setItem("productsInCart", JSON.stringify(data));
    this.setState({ openedModalStatus: false, productsInCart: data.length });
    this.props.updateCart(1)
  };

  removeFromCart = () => {
    const data = JSON.parse(localStorage.getItem("productsInCart"));
    const productToRemoveIdx = data.findIndex(
      (obj) => obj.vendorCode === this.state.selectedProduct.vendorCode
    );

    if (productToRemoveIdx !== -1) {
      data.splice(productToRemoveIdx, 1);
    }

    localStorage.setItem("productsInCart", JSON.stringify(data));
    this.setState({ productsInCart: data.length });
    this.props.updateCart(- 1)
  };

  addToFavor = (product) => {
    const data = JSON.parse(localStorage.getItem("productsInFavor"));
    data.push(product);
    localStorage.setItem("productsInFavor", JSON.stringify(data));
    this.setState({ productsInFavor: data.length });
    this.props.updateFavor(1)
  };

  removeFromFavor = (product) => {
    const data = JSON.parse(localStorage.getItem("productsInFavor"));
    const productToRemoveIdx = data.findIndex(
      (obj) => obj.vendorCode === product.vendorCode
    );

    if (productToRemoveIdx !== -1) {
      data.splice(productToRemoveIdx, 1);
    }

    localStorage.setItem("productsInFavor", JSON.stringify(data));
    this.setState({ productsInFavor: data.length });
    this.props.updateFavor(-1)
  };

  render() {
    const cart = JSON.parse(localStorage.getItem("productsInCart"));
    const favor = JSON.parse(localStorage.getItem("productsInFavor"));
    return (
      <>
        <div className="products-list">
          {this.state.productsArr.map((product, id) => (
            <Product
              inCart={
                cart.findIndex(
                  (obj) => obj.vendorCode === product.vendorCode
                ) !== -1
              }
              inFavor={
                favor.findIndex(
                  (obj) => obj.vendorCode === product.vendorCode
                ) !== -1
              }
              removeFromCart={this.removeFromCart}
              addToFavor={this.addToFavor}
              openModal={this.openModal}
              removeFromFavor={this.removeFromFavor}
              key={id}
              title={product.title}
              url={product.url}
              vendorCode={product.vendorCode}
              color={product.color}
              price={Number(product.price)}
            />
          ))}
        </div>
        {this.state.openedModalStatus ? (
          <Modal
            title="Confirm adding"
            content="Do you want to add a product to your cart?"
            addProduct={this.addToCart}
            closeModal={this.closeModal}
            btns={["Confirm", "Cancel"]}
          />
        ) : null}
      </>
    );
  }
}
