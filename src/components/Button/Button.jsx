import PropTypes from "prop-types";

export default function Button(props) {
  const { text, className } = props;
  return (
    <button className={className} onClick={props.onClick}>
      {text}
    </button>
  );
}

Button.propTypes = {
  text: PropTypes.string.isRequired,
  className: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
};
