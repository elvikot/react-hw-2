import { Component } from "react";
import { faStar } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import "./Favor.scss";
import PropTypes from "prop-types";

export default class Favor extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { productsInFavorAmount } = this.props;

    return (
      <div className="favor-wrapper">
        <p className="counter">{productsInFavorAmount}</p>
        <FontAwesomeIcon icon={faStar} className="test" />
      </div>
    );
  }
}

Favor.propTypes = {
  productsInFavorAmount: PropTypes.number.isRequired,
};
